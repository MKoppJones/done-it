export default interface Category {
    categoryID: number;
    name: string;
    color: string;
  }
