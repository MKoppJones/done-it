import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import TaskItem from '../TaskItem';
import Timer from '../Timer';
import Formatters from "../Formatters";

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  @Input() tasks: TaskItem[];
  @Input() timer: Timer;
  @Input() trackedTaskIndex: number;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onToggleTaskTracking = new EventEmitter<number>();
  constructor() {}

  ngOnInit() {}

  toggleTaskTracking() {
    this.onToggleTaskTracking.emit(this.trackedTaskIndex);
  }

  formatTime(elapsed: number, seconds: boolean) {
    return Formatters.formatTime(elapsed, seconds);
  }

  normaliseTime(milli: number) {
    return Formatters.normaliseTime(milli);
  }

}
