import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import TaskItem from '../TaskItem';
import Category from '../Category';
import Formatters from '../Formatters';

@Component({
  selector: 'app-archive-viewer',
  templateUrl: './archive-viewer.component.html',
  styleUrls: ['./archive-viewer.component.css']
})
export class ArchiveViewerComponent implements OnInit {
  @Input() todos: TaskItem[];
  @Input() categories: Category[];
  // tslint:disable-next-line:no-output-on-prefix
  @Output() toggleTaskArchive = new EventEmitter<number>();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onUnarchiveTask = new EventEmitter<number>();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() deleteTask = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  get archivedTasks() {
    return this.todos.filter((task, index) => {
      return task.isArchived;
    });
  }

  getCategoryChar(id: number) {
    if (this.categories.length === 0) {
      return "";
    }
    if (id == null) {
      return "";
    }
    const cat = this.categories.filter(x => x.categoryID === id)[0];
    if (cat.name == null) {
      return "";
    }
    return cat.name.charAt(0);
  }

  getColor(categoryID: number) {
    const cat = this.categories.find(x => x.categoryID === categoryID);
    if (cat) {
      return cat.color;
    }
    return "";
  }

  formatDate(milli: number) {
    return Formatters.formatDate(milli);
  }

  formatTime(elapsed: number, seconds: boolean) {
    return Formatters.formatTime(elapsed, seconds);
  }

  normaliseTime(milli: number) {
    return Formatters.normaliseTime(milli);
  }

}
