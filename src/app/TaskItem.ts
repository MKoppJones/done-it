export default interface TaskItem {
  _id: string;
  id: number;
  title: string;
  isComplete: boolean;
  description: string;
  completionTime: any;
  isArchived: boolean;
  elapsedTime: number;
  categoryID: any;
  color: string;
}
