import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

import TaskItem from "../TaskItem";
import Formatters from "../Formatters";
import Category from "../Category";
import Timer from "../Timer";
import Settings from "../Settings";

@Component({
  selector: "app-todo-list",
  templateUrl: "./todo-list.component.html",
  styleUrls: ["./todo-list.component.css"]
})
export class TodoListComponent implements OnInit {
  ddlVisible = false;
  filteredCategories = [];
  @Input() todos: TaskItem[];
  @Input() activeTask: number;
  @Input() timer: Timer;
  @Input() highlightTask: number;
  @Input() categories: Category[];
  @Input() settings: Settings;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onArchiveTask = new EventEmitter<number>();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onToggleTaskComplete = new EventEmitter<number>();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onToggleTaskTracking = new EventEmitter<number>();
  @Output() toggleTaskDetails = new EventEmitter();

  getCategoryChar(id: number) {
    if (this.categories.length === 0) {
      return "";
    }
    if (id == null) {
      return "";
    }
    const cat = this.categories.filter(x => x.categoryID === id)[0];
    if (cat.name == null) {
      return "";
    }
    return cat.name.charAt(0);
  }

  get activeTasks() {
    return this.todos.filter((task, index) => {
      if (this.filteredCategories.length > 0) {
        return (
          !task.isArchived && this.filteredCategories.includes(task.categoryID)
        );
      } else {
        return !task.isArchived;
      }
    });
  }

  constructor() {}

  ngOnInit() {}

  toggleCategoryFilter() {
    this.ddlVisible = !this.ddlVisible;
  }

  toggleFilter(e, category) {
    const checkbox = e.target.getElementsByTagName("input")[0];
    checkbox.checked = !checkbox.checked;

    if (checkbox.checked) {
      e.target.classList.remove("unticked");
      this.filteredCategories.push(category);
    } else {
      e.target.classList.add("unticked");
      this.filteredCategories.splice(
        this.filteredCategories.indexOf(category),
        1
      );
    }
  }

  toggleTaskTracking(index: number) {
    this.onToggleTaskTracking.emit(index);
  }

  toggleTaskComplete(index: number) {
    this.onToggleTaskComplete.emit(index);
  }

  archiveTodo(index: number) {
    this.onArchiveTask.emit(index);
  }

  getColor(categoryID: number) {
    const cat = this.categories.find(x => x.categoryID === categoryID);
    if (cat) {
      return cat.color;
    }
    return "";
  }

  formatDate(milli: number) {
    return Formatters.formatDate(milli);
  }

  formatTime(elapsed: number, seconds: boolean) {
    return Formatters.formatTime(elapsed, seconds);
  }

  normaliseTime(milli: number) {
    return Formatters.normaliseTime(milli);
  }
}
