export default interface Settings {
  defaultSavePath: string;
  defaultCategory: string;
  showNormalTime: string;
}
