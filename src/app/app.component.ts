import { Component, OnInit } from "@angular/core";
import TaskItem from "./TaskItem";
import Timer from "./Timer";
import LogEvent from "./LogEvent";
import * as Datastore from "nedb";
import Formatters from "./Formatters";
import Category from "./Category";
import Settings from "./Settings";

declare var fs: any;
declare var shell: any;
declare var dialog: any;
declare var globalShortcut: any;
declare var Notification: any;
declare var notifier: any;
declare var dir: any;
declare var remote: any;
declare var app: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  constructor() {}
  todos: TaskItem[] = [];
  trackedTaskIndex: number = null;
  timer: Timer = {
    taskIndex: null,
    started: 0,
    timer: null,
    elapsed: 0,
    running: false,
    lastTick: 0,
    notifier: null
  };
  selectedCategory: Category = {
    categoryID: null,
    name: null,
    color: null
  };
  loggedEvents: LogEvent[] = [];
  isDataLoaded = false;
  timeDB = new Datastore({
    filename: "./time.db",
    autoload: true
  });
  tasksDB = new Datastore({
    filename: "./tasks.db",
    autoload: true
  });
  categoryDB = new Datastore({
    filename: "./categories.db",
    autoload: true
  });
  hoverTaskID: number;
  hoverTimer = null;
  categories: Category[] = [];

  confirmation = {
    positiveCallback: null,
    negativeCallback: null,
    title: "",
    detail: "",
    positiveButton: "",
    negativeButton: ""
  };

  alert = {
    title: "",
    detail: ""
  };

  settings: Settings = {
    defaultSavePath: "",
    defaultCategory: "",
    showNormalTime: "false"
  };

  selectedTaskDetail: TaskItem = null;

  ret = globalShortcut.register("CommandOrControl+\\", () => {
    this.onToggleTaskTracking(this.trackedTaskIndex);
  });

  isTimer: boolean = false;

  confirmationBox(
    title,
    detail,
    positiveText,
    negativeText,
    positiveCallback,
    negativeCallback
  ) {
    this.confirmation = {
      positiveCallback: positiveCallback,
      negativeCallback: negativeCallback,
      title: title,
      detail: detail,
      positiveButton: positiveText,
      negativeButton: negativeText
    };
    document
      .getElementById("confirmation-box-backer")
      .classList.add("blocking");
    document.getElementById("confirmation-box").classList.remove("closed");
  }

  externalAlertBox(alertDetails) {
    this.alertBox(alertDetails.title, alertDetails.detail);
  }

  alertBox(title, detail) {
    this.alert.title = title;
    this.alert.detail = detail;
    this.toggleAlert();
  }

  confirmationPositiveCallback() {
    this.closeConfirmationBox();
    if (this.confirmation.positiveCallback != null) {
      this.confirmation.positiveCallback();
    }
  }

  confirmationNegativeCallback() {
    this.closeConfirmationBox();
    if (this.confirmation.negativeCallback != null) {
      this.confirmation.negativeCallback();
    }
  }

  closeConfirmationBox() {
    document.getElementById("confirmation-box").classList.add("closed");
    document
      .getElementById("confirmation-box-backer")
      .classList.remove("blocking");
  }

  highlightTask(taskID: number) {
    this.hoverTaskID = taskID;

    if (this.hoverTimer != null) {
      clearTimeout(this.hoverTimer);
    }

    this.hoverTimer = setTimeout(() => {
      this.hoverTaskID = null;
    }, 15000);
  }

  initDBs() {
    // Load inital task list.
    this.tasksDB
      .find()
      .sort({ id: 1 })
      .exec((_err: any, docs: TaskItem[]) => {
        this.todos = docs;
        this.isDataLoaded = true;
        this.tasksDB.persistence.setAutocompactionInterval(0);
      });

    // Load log.
    this.timeDB
      .find()
      .sort({ dateTimeLogged: -1 })
      .exec((_err: any, docs: LogEvent[]) => {
        this.loggedEvents = docs;
        this.timeDB.persistence.setAutocompactionInterval(0);
      });

    this.categoryDB
      .find()
      .sort({ id: -1 })
      .exec((_err: any, docs: Category[]) => {
        this.categories = docs;
        this.categoryDB.persistence.setAutocompactionInterval(0);
        this.readSettings();
      });
  }

  ngOnInit() {
    this.initDBs();
  }

  // TODO: Add limit here to avoid infinite loop.
  readSettings() {
    fs.readFile("settings.json", "utf8", (err, data) => {
      if (err) {
        this.saveSettings(null);
      } else {
        this.settings = JSON.parse(data);
      }
      if (this.settings.defaultCategory.length != 0) {
        if (
          this.categories.length > 0 &&
          JSON.stringify(this.selectedCategory) ===
            JSON.stringify({
              categoryID: null,
              name: null,
              color: null
            })
        ) {
          this.selectedCategory = this.categories.filter(
            c => c.categoryID === parseInt(this.settings.defaultCategory, 10)
          )[0];
        } else {
          this.settings.defaultCategory = "";
          this.saveSettings(this.settings);
        }
      }
    });
  }

  saveSettings(settings) {
    if (settings) {
      this.settings = settings;
    } else {
      this.settings = {
        defaultSavePath: "",
        defaultCategory: "",
        showNormalTime: "false"
      };
    }

    fs.writeFile(
      "settings.json",
      JSON.stringify(this.settings),
      "utf8",
      err => {
        if (err) {
          throw err;
        }
        if (!settings) {
          this.readSettings();
        }
      }
    );
  }

  createCategory(category: Category) {
    this.categoryDB.insert(category, (_err: any, _numReplaced: any) => {
      this.refreshCategories("b");
    });
  }

  onCreateTask(newTask: any) {
    const category = Object.assign({}, newTask.category);
    newTask = { ...newTask.newTask, id: this.todos.length };
    if (category.categoryID != null) {
      newTask.categoryID = category.categoryID;
    }
    if (category.categoryID == null && category.name != null) {
      category.categoryID = this.categories.length;
      this.createCategory(category);
      newTask.categoryID = category.categoryID;
    }

    this.tasksDB.insert({
      id: newTask.id,
      title: newTask.title,
      isComplete: newTask.isComplete,
      description: newTask.description,
      completionTime: newTask.completionTime,
      isArchived: newTask.isArchived,
      elapsedTime: newTask.elapsedTime,
      categoryID: newTask.categoryID,
      color: newTask.color
    });
    this.refreshTasks();
  }

  killRunningTimer(_index: number) {
    clearInterval(this.timer.timer._id);
    clearInterval(this.timer.notifier._id);
    this.logElapsedTimeForTask();
    this.timer.taskIndex = this.trackedTaskIndex = null;
    this.timer.timer = null;
    this.timer.running = false;
  }

  logElapsedTimeForTask() {
    this.todos[this.timer.taskIndex].elapsedTime = Math.floor(
      this.todos[this.timer.taskIndex].elapsedTime + this.timer.elapsed
    );
    this.tasksDB.update(
      { _id: this.todos[this.timer.taskIndex]._id },
      this.todos[this.timer.taskIndex],
      { multi: true },
      (_err, _numReplaced) => {
        this.refreshTasks();
      }
    );
    const timeEvent = {
      taskID: this.timer.taskIndex,
      timeSpent: this.timer.elapsed,
      dateTimeLogged: Date.now()
    };
    this.timeDB.insert(timeEvent);
    this.refreshLog();
    this.timer.elapsed = 0;
  }

  startNewTaskTimer(index: number) {
    this.timer.taskIndex = this.trackedTaskIndex = index;
    this.timer.started = performance.now();
    this.timer.lastTick = performance.now();
    this.timer.running = true;
    this.timer.timer = setInterval(() => {
      if (this.timer.running) {
        this.timer.elapsed += performance.now() - this.timer.lastTick;
        this.timer.lastTick = performance.now();
      }
    }, 1000);

    this.timer.notifier = setInterval(() => {
      if (this.timer.running) {
        notifier.notify({
          title: "Todo",
          message: `A timer is running for ${
            this.todos[this.timer.taskIndex].title
          }`,
          icon: `${dir}/dist/angular-electron/assets/logo.png`,
          timeout: 5
        });
      }
    }, 300000); // 5 mins

    notifier.notify({
      title: "Todo - Timer Started",
      message: `${this.todos[this.timer.taskIndex].title}`,
      icon: `${dir}/dist/angular-electron/assets/logo.png`,
      timeout: 5
    });
  }

  onToggleTaskTracking(index: number) {
    if (this.timer.taskIndex !== null) {
      if (index !== this.timer.taskIndex) {
        this.killRunningTimer(this.timer.taskIndex);
      } else {
        this.timer.running = !this.timer.running;
        if (!this.timer.running) {
          this.logElapsedTimeForTask();

          notifier.notify({
            title: "Todo - Timer Stopped",
            message: `${this.todos[this.timer.taskIndex].title}`,
            icon: `${dir}/dist/angular-electron/assets/logo.png`,
            timeout: 5
          });
        } else {
          this.timer.lastTick = performance.now();
        }
        return;
      }
    }

    this.startNewTaskTimer(index);
  }

  onToggleTaskComplete(index: number) {
    this.todos[index].isComplete = !this.todos[index].isComplete;
    if (this.todos[index].isComplete) {
      this.todos[index].completionTime = Date.now();
    } else {
      this.todos[index].completionTime = null;
    }
  }

  onArchiveTask(index: number) {
    this.todos[index].isArchived = true;
    this.tasksDB.update(
      { _id: this.todos[index]._id },
      this.todos[index],
      { multi: true },
      (_err, _numReplaced) => {
        this.refreshTasks();
      }
    );
  }

  onUnarchiveTask(index: number) {
    this.todos[index].isArchived = false;
    this.tasksDB.update(
      { _id: this.todos[index]._id },
      this.todos[index],
      { multi: true },
      (_err, _numReplaced) => {
        this.refreshTasks();
      }
    );
  }

  queryDeleteTask(id: number) {
    this.confirmationBox(
      "Are you sure you want to delete this task?",
      "Deleting this task will also remove all time logged against it.",
      "Yes",
      "No",
      () => this.deleteTask(id),
      null
    );
  }

  deleteTask(id: number) {
    this.tasksDB.remove(
      { _id: this.todos.filter(x => x.id === id)[0]._id },
      { multi: true },
      (_err, _numReplaced) => {
        this.refreshTasks();
      }
    );
  }

  queryRemoveAllTasks() {
    this.confirmationBox(
      "Are you sure you want to remove all tasks?",
      "This will also remove all of the logged time events for them. You can view your archives by going to ...",
      "Yes",
      "No",
      this.removeAllTasks(),
      null
    );
  }

  removeAllTasks() {
    this.removeAllTime(false);
    this.tasksDB.remove({}, { multi: true }, (_err, _docs) => {
      this.refreshTasks();
    });
  }

  queryRemoveAllTimeEvents() {
    this.confirmationBox(
      "Are you sure you want to remove all logged time events?",
      "",
      "Yes",
      "No",
      () => this.removeAllTime(true),
      null
    );
  }

  removeAllTime(doQuery: boolean) {
    if (doQuery) {
      this.confirmationBox(
        "Would you like to remove the logged time on tasks too?",
        "",
        "Yes",
        "No",
        () => {
          this.tasksDB.update(
            {},
            { $set: { elapsedTime: 0 } },
            { multi: true },
            (_err: any, _numReplaced: number) => {
              this.refreshTasks();
              this.timeDB.remove({}, { multi: true }, (__err, _docs) => {
                this.refreshLog();
              });
            }
          );
        },
        null
      );
    } else {
      this.timeDB.remove({}, { multi: true }, (_err, _docs) => {
        this.refreshLog();
      });
    }
  }

  queryRemoveAllCategories() {
    this.confirmationBox(
      "Would you like to remove all categories?",
      "This will remove the categories from the tasks too.",
      "Yes",
      "No",
      this.removeAllCategories(),
      null
    );
  }

  removeAllCategories() {
    this.tasksDB.update(
      {},
      { $set: { categoryID: null } },
      { multi: true },
      (_err: any, _numReplaced: number) => {
        this.refreshTasks();
        this.categoryDB.remove({}, { multi: true }, (__err, _docs) => {
          this.refreshCategories("c");
        });
      }
    );
  }

  removeEverything() {
    this.confirmationBox(
      "Are you sure you want to start fresh?",
      "This will delete everything that is stored.",
      "Yes",
      "No",
      () => {
        {
          this.removeAllTasks();
          this.removeAllCategories();
        }
      },
      null
    );
  }

  refreshTasks() {
    this.tasksDB
      .find()
      .sort({ id: 1 })
      .exec((_err, docs) => {
        this.todos = docs;
      });
  }

  refreshLog() {
    this.timeDB
      .find()
      .sort({ dateTimeLogged: -1 })
      .exec((_err, docs) => {
        this.loggedEvents = docs;
      });
  }

  analyseToday() {
    const today = new Date(new Date().setHours(0, 0, 0, 0));
    const analysed = {};
    this.timeDB.find(
      { dateTimeLogged: { $gt: today.getTime() } },
      (_err, docs) => {
        const docCount = docs.length;
        for (let i = 0; i < docCount; i++) {
          const taskName = this.todos.find(t => t.id === docs[i].taskID).title;
          if (!analysed[taskName]) {
            analysed[taskName] = 0;
          }
          analysed[taskName] = analysed[taskName] + docs[i].timeSpent;
        }
        this.saveAnalysedTasks(analysed);
      }
    );
  }

  refreshCategories(caller) {
    this.categoryDB
      .find()
      .sort({ name: 1 })
      .exec((_err, docs) => {
        this.categories = docs;
      });
  }

  saveAnalysedTasks(analysed) {
    for (const key in analysed) {
      if (analysed.hasOwnProperty(key)) {
        analysed[key] = this.normaliseTime(analysed[key]);
      }
    }
    fs.writeFile(
      "analysedTasks.json",
      JSON.stringify(analysed, null, 2),
      () => {
        // TODO: Convert this to custom confimration box
        dialog.showMessageBox(
          {
            type: "question",
            buttons: ["Yes", "No"],
            title: "Show created file?",
            message:
              "Would you like to see the analysed time file now? NOTE: This will open in your default text editor."
          },
          function(response) {
            if (response === 0) {
              shell.openItem("analysedTasks.json");
            }
          }
        );
      }
    );
  }

  toggleDrawer(e) {
    e.target.parentElement.classList.toggle("open");
    document.getElementById("slideout-backer").classList.toggle("blocking");
  }

  toggleManageCategories() {
    document.getElementById("category-maintaner").classList.toggle("closed");
    document
      .getElementById("category-maintaner-backer")
      .classList.toggle("blocking");
  }

  deleteCategory(catID: number) {
    this.confirmationBox(
      "Are you sure you want to delete this category?",
      "This will remove it from all tasks that currently have it.",
      "Yes",
      "No",
      () =>
        this.tasksDB.update(
          { categoryID: catID },
          { $set: { categoryID: null } },
          { multi: true },
          (_err: any, _numReplaced: number) => {
            this.refreshTasks();
            this.categoryDB.remove(
              { categoryID: catID },
              {},
              (__err: any, __numReplaced: number) => this.refreshCategories("a")
            );
          }
        ),
      null
    );
  }

  formatDate(milli: number) {
    return Formatters.formatDate(milli);
  }

  formatTime(elapsed: number, seconds: boolean) {
    return Formatters.formatTime(elapsed, seconds);
  }

  normaliseTime(milli: number) {
    return Formatters.normaliseTime(milli);
  }

  toggleCredits() {
    document.getElementById("credits").classList.toggle("closed");
    document.getElementById("about-backer").classList.toggle("blocking");
  }

  toggleAlert() {
    document.getElementById("alert-title").innerText = this.alert.title;
    document.getElementById("alert-detail").innerText = this.alert.detail;
    document.getElementById("alert-box").classList.toggle("closed");
    document.getElementById("alert-box-backer").classList.toggle("blocking");
  }

  toggleTaskBackup() {
    dialog.showSaveDialog(
      remote.BrowserWindow.getFocusedWindow(),
      {
        defaultPath:
          this.settings.defaultSavePath +
          "done-it-dot-backup-" +
          new Date().getTime() +
          ".json",
        filters: [{ name: "JSON", extensions: [".json"] }]
      },
      (filename, _other) => {
        fs.writeFile(filename, this.generateBackupFile(), () => {
          this.alertBox(
            "Backup successfully created",
            `Backup file created at ${filename}`
          );
        });
      }
    );
  }

  generateBackupFile() {
    const backupContent = {
      tasks: [],
      categories: [],
      loggedEvents: []
    };
    backupContent.tasks = this.todos;
    backupContent.categories = this.categories;
    backupContent.loggedEvents = this.loggedEvents;
    return JSON.stringify(backupContent, null, 2);
  }

  toggleTaskRestore() {
    dialog.showOpenDialog(
      remote.BrowserWindow.getFocusedWindow(),
      {
        filters: [{ name: "JSON", extensions: ["json"] }],
        defaultPath: this.settings.defaultSavePath,
        buttonLabel: "Restore",
        properties: ["openFile"]
      },
      filename => {
        fs.readFile(filename[0], "utf8", (err, data) => {
          if (err) {
            throw err;
          }

          this.selectedCategory = {
            categoryID: null,
            name: null,
            color: null
          };

          data = JSON.parse(data);

          // TODO: Refactor this.
          this.timeDB.remove({}, { multi: true }, () => {
            this.tasksDB.remove({}, { multi: true }, () => {
              this.categoryDB.remove({}, { multi: true }, () => {
                this.categoryDB.insert(data.categories, () => {
                  this.categoryDB.find().exec((_err, cats) => {
                    this.categories = cats;
                    this.tasksDB.insert(data.tasks, () => {
                      this.tasksDB.find().exec((__err, tasks) => {
                        this.todos = tasks;
                        this.timeDB.insert(data.loggedEvents, () => {
                          this.timeDB.find().exec((___err, logs) => {
                            this.loggedEvents = logs;
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      }
    );
  }

  toggleTaskArchive() {
    document.getElementById("task-archive").classList.toggle("closed");
    document.getElementById("task-archive-backer").classList.toggle("blocking");
  }

  toggleSettings() {
    document.getElementById("settings").classList.toggle("closed");
    document.getElementById("settings-backer").classList.toggle("blocking");
  }

  toggleTaskDetails(task: TaskItem) {
    this.refreshTasks();
    this.selectedTaskDetail = task;
    document.getElementById("task-detail").classList.toggle("closed");
    document.getElementById("task-detail-backer").classList.toggle("blocking");
  }

  minimiseApp() {
    remote.BrowserWindow.getFocusedWindow().minimize();
  }

  timerWindow() {
    if (this.isTimer) {
      remote.BrowserWindow.getFocusedWindow().setMinimumSize(965, 500);
      remote.BrowserWindow.getFocusedWindow().setMaximumSize(1920, 1080);
      remote.BrowserWindow.getFocusedWindow().setSize(965, 500, true);
      this.isTimer = false;
      remote.BrowserWindow.getFocusedWindow().setMaximizable(true);
      remote.BrowserWindow.getFocusedWindow().setAlwaysOnTop(false);
      document.getElementsByClassName("timer")[0].innerHTML = "⏰";
    } else {
      this.isTimer = true;
      remote.BrowserWindow.getFocusedWindow().setMinimumSize(300, 140);
      remote.BrowserWindow.getFocusedWindow().setMaximumSize(300, 140);
      remote.BrowserWindow.getFocusedWindow().setSize(300, 140, true);
      remote.BrowserWindow.getFocusedWindow().setAlwaysOnTop(true);
      remote.BrowserWindow.getFocusedWindow().setMaximizable(false);
      document.getElementsByClassName("timer")[0].innerHTML = "📋";
    }
  }

  maximiseApp() {
    if (!remote.BrowserWindow.getFocusedWindow().isMaximizable()) {
      return;
    }

    if (remote.BrowserWindow.getFocusedWindow().isMaximized()) {
      remote.BrowserWindow.getFocusedWindow().unmaximize();
      document.getElementsByClassName("maximise")[0].innerHTML = "🗖";
    } else {
      remote.BrowserWindow.getFocusedWindow().maximize();
      document.getElementsByClassName("maximise")[0].innerHTML = "🗗";
    }
  }

  closeApp() {
    // TODO: Clear down and save any active tasks.
    app.quit();
  }

  reportIssue() {
    shell.openExternal("https://gitlab.com/MKoppJones/done-it/issues");
  }

  doUpdateTask(task: TaskItem) {
    const originalTask = this.todos.filter(x => (x.id = task.id))[0];
    const index = this.todos.indexOf(originalTask);

    this.tasksDB.update(
      { _id: task._id },
      task,
      { multi: true },
      (_err, _numReplaced) => {
        this.refreshTasks();
        this.alertBox(
          "Task updated",
          "Your task changes were successfully saved."
        );
      }
    );
  }
}
