export default interface Timer {
  taskIndex: number;
  started: number;
  running: boolean;
  lastTick: number;
  timer: any;
  elapsed: number;
  notifier: any;
}
