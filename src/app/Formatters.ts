const Formatters = {
  normaliseTime(elapsed: number) {
    return (elapsed / 3600000).toFixed(2);
  },
  formatTime(elapsed: number, showSeconds: boolean) {
    const seconds = Math.floor((elapsed / 1000) % 60);
    const minutes = Math.floor((elapsed / 60000) % 60);
    const hours = Math.floor(elapsed / 3600000);
    const res =
      hours +
      ":" +
      ("0" + minutes).slice(-2) +
      (showSeconds ? "." + ("0" + seconds).slice(-2) : "");
    return res;
  },
  formatDate(milli: number, withTime: boolean = true) {
    if (milli === 0 || milli === null) {
      return "";
    }
    const d = new Date(milli);
    let month = (d.getMonth() + 1).toString();
    let day = d.getDate().toString();
    const year = d.getFullYear();

    let hours = d.getHours().toString();
    let minutes = d.getMinutes().toString();

    if (month.length < 2) {
      month = "0" + month;
    }

    if (day.length < 2) {
      day = "0" + day;
    }

    if (hours.length < 2) {
      hours = "0" + hours;
    }

    if (minutes.length < 2) {
      minutes = "0" + minutes;
    }

    const datePart = [year, month, day].join("-");
    return datePart + (withTime ? " " + [hours, minutes].join(":") : "");
  }
};

export default Formatters;
