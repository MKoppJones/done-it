import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import TaskItem from "../TaskItem";
import Category from "../Category";
import Formatters from "../Formatters";
import Settings from "../Settings";
import LogEvent from "../LogEvent";

@Component({
  selector: "app-task-detail",
  templateUrl: "./task-detail.component.html",
  styleUrls: ["./task-detail.component.css"]
})
export class TaskDetailComponent implements OnInit {
  @Input() settings: Settings;
  @Input() tasks: TaskItem[] = [];
  @Input() set selectedTask(task: TaskItem) {
    if (task) {
      this.task = task;
    } else {
      this.task = {
        _id: "",
        id: 0,
        title: "",
        isComplete: false,
        description: "",
        completionTime: "",
        isArchived: false,
        elapsedTime: 0,
        categoryID: "",
        color: ""
      };
    }
  }
  @Input() categories: Category[];
  @Input() set loggedEvents(events: LogEvent[]) {
    let today = new Date();
    today.setHours(0, 0, 0, 0);
    // TODO: Only use today events.
    if (events.length > 0) {
      this.totalLoggedToday = events.reduce((p, c) => {
        if (today.getTime() <= c.dateTimeLogged) {
          p.timeSpent = p.timeSpent + c.timeSpent;
        }
        return p;
      }).timeSpent;
    } else {
      this.totalLoggedToday = 0;
    }
  }
  @Output() toggleTaskDetails = new EventEmitter();
  @Output() doUpdateTask = new EventEmitter<TaskItem>();

  totalLoggedToday = 0;

  task: TaskItem = {
    _id: "",
    id: 0,
    title: "",
    isComplete: false,
    description: "",
    completionTime: "",
    isArchived: false,
    elapsedTime: 0,
    categoryID: "",
    color: ""
  };

  constructor() { }

  ngOnInit() { }

  getCategoryColor(id: number) {
    if (id !== null && id.toString().trim().length > 0) {
      return this.categories.filter(x => x.categoryID === id)[0].color;
    }
    return "null";
  }

  totalToday() {
    return this.totalLoggedToday;
  }

  formatDate(milli: number) {
    return Formatters.formatDate(milli);
  }

  formatTime(elapsed: number, seconds: boolean) {
    return Formatters.formatTime(elapsed, seconds);
  }

  normaliseTime(milli: number) {
    return Formatters.normaliseTime(milli);
  }
}
