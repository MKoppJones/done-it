import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import TaskItem from "../TaskItem";
import Category from "../Category";
import Settings from "../Settings";

@Component({
  selector: "app-todo-form",
  templateUrl: "./todo-form.component.html",
  styleUrls: ["./todo-form.component.css"]
})
export class TodoFormComponent implements OnInit {
  newTask: TaskItem = {
    _id: null,
    id: 0,
    title: "",
    isComplete: false,
    description: "",
    completionTime: 0,
    isArchived: false,
    elapsedTime: 0,
    categoryID: null,
    color: null
  };

  selectedCategory: any;

  @Input() set selectedCategorySetter(cat: any) {
    this.selectedCategory = cat;
  }

  cats: Category[] = [];

  @Input()
  set categories(value: Category[]) {
    if (value.length > 0) {
      this.cats = value;
      if (
        this.selectedCategory.categoryID != null ||
        this.selectedCategory.name != null ||
        this.selectedCategory.color != null
      ) {
        if (this.selectedCategory.name != null) {
          this.selectedCategory.categoryID = this.cats.filter(
            x =>
              x.name === this.selectedCategory.name &&
              x.color === this.selectedCategory.color
          )[0].categoryID;
        }
      }
    } else {
      this.cats = [];
    }
  }

  pickerVisible = false;
  ddlVisible = false;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onCreateTask = new EventEmitter<any>();

  ngOnInit() {}

  validateNewTask() {
    return this.newTask.title.length > 0;
  }

  onSubmit() {
    const isValid = this.validateNewTask();
    if (!isValid) {
      return;
    }
    this.onCreateTask.emit({
      newTask: this.newTask,
      category: this.selectedCategory
    });
    this.newTask = {
      _id: null,
      id: 0,
      title: "",
      isComplete: false,
      description: "",
      completionTime: 0,
      isArchived: false,
      elapsedTime: 0,
      categoryID: 0,
      color: ""
    };

    // this.selectedCategory = {
    //   categoryID: null,
    //   name: null,
    //   color: null
    // };
  }

  setCategoryColour(color) {
    this.selectedCategory.color = color;
    this.selectedCategory.categoryID = null;
  }

  setCategory(cat) {
    this.selectedCategory = Object.assign({}, this.cats[cat]);
  }

  updateTaskName(value) {
    this.selectedCategory.categoryID = null;
    this.selectedCategory.name = value;
  }

  toggleColorPicker() {
    this.pickerVisible = !this.pickerVisible;
  }

  toggleDdlPicker() {
    this.ddlVisible = !this.ddlVisible;
  }
}
