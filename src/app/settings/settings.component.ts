import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import Settings from "../Settings";
import { tryParse } from "selenium-webdriver/http";
import Category from '../Category';
declare var fs: any;

@Component({
  selector: "app-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.css"]
})
export class SettingsComponent implements OnInit {
  currentSettings: Settings = {
    defaultSavePath: "",
    defaultCategory: "",
    showNormalTime: "false"
  };

  @Input()
  set settings(value: Settings) {
    this.currentSettings = value;
  }

  @Input() categories: Category[] = [];

  @Output() toggleSettings = new EventEmitter();
  @Output() queryRemoveAllCategories = new EventEmitter();
  @Output() queryRemoveAllTimeEvents = new EventEmitter();
  @Output() queryRemoveAllTasks = new EventEmitter();
  @Output() removeEverything = new EventEmitter();
  @Output() saveSettings = new EventEmitter<any>();
  @Output() alertBox = new EventEmitter<any>();

  constructor() { }

  ngOnInit() { }

  settingsTabChange(element, index) {
    let elementList = document.querySelectorAll("#settings .tabs li");
    for (let i = 0; i < elementList.length; i++) {
      elementList[i].classList.remove("is-active");
    }
    elementList = document.querySelectorAll(".settings-tab");
    for (let i = 0; i < elementList.length; i++) {
      (<HTMLElement>elementList[i]).style.display = "none";
    }
    element.target.parentElement.parentElement.classList.add("is-active");
    document.getElementById(`settings-tab${index}`).style.display = "";
  }

  startSaveSettings() {
    if (this.validateSettings()) {
      this.saveSettings.emit(this.currentSettings);
      this.alertBox.emit({
        title: "Settings Saved",
        detail: ""
      });
    }
  }

  validateSettings() {
    if (this.currentSettings.defaultSavePath.trim.length > 0) {
      try {
        fs.lstatSync(this.currentSettings.defaultSavePath).isDirectory();
        if (this.currentSettings.defaultSavePath.slice(-1) !== "\\") {
          this.currentSettings.defaultSavePath =
            this.currentSettings.defaultSavePath + "\\";
        }
      } catch (e) {
        this.alertBox.emit({
          title: "Directory Does Not Exists",
          detail:
            // tslint:disable-next-line:max-line-length
            "The directory chosen for the default backup directory does not exist, or Done It. does not have permission to read that directory."
        });
        return false;
      }
    }
    return true;
  }

  getCategoryColor(id: string) {
    if (this.categories.length == 0 || this.categories == null) return "null";
    if (id !== null && id.toString().trim().length > 0) {
      return this.categories.filter(x => x.categoryID === parseInt(id))[0].color;
    }
    return "null";
  }
}
