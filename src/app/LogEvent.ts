export default interface LogEvent {
    taskID: number;
    timeSpent: number;
    dateTimeLogged: number;
  }
