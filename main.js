const { app, BrowserWindow, Tray } = require("electron");

app.setAppUserModelId('com.devmade.todo');

let win;
let timerWin;

function createWindow() {
  tray = new Tray(`${__dirname}/dist/angular-electron/assets/logo.png`);
  tray.setToolTip("test");

  // Create the browser window.
  win = new BrowserWindow({
    width: 1000,
    height: 600,
    backgroundColor: "#ffffff",
    icon: `${__dirname}/dist/angular-electron/assets/logo.png`,
    frame: false
  });

  // timerWin = new BrowserWindow({
  //   width: 600,
  //   height: 600,
  //   backgroundColor: '#ffffff',
  //   icon: `file://${__dirname}/dist/angular-electron/assets/logo.png`
  // });

  win.loadURL(`file://${__dirname}/dist/angular-electron/index.html`);
  //timerWin.loadURL(`file://${__dirname}/dist/angular-electron/index.html`);

  // Event when the window is closed.
  win.on("closed", function() {
    win = null;
  });

  // // Event when the window is closed.
  // timerWin.on('closed', function () {
  //   timerWin = null
  // })
}

// Create window on electron intialization
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", function() {
  // On macOS specific close process
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", function() {
  // macOS specific close process
  if (win === null) {
    createWindow();
  }
});
